# LVMB

Esta es una implementación de un blog basico donde se puede crear y editar posts.
### Herramientas
  - Laravel 5.4
  - Vue.js 2
  - MongoDB

# Pre-Instalación!
Necesitas contar con mongoDB instalado y descargar la extensión de MongoDB para PHP. 
[mongodb](https://pecl.php.net/package/mongodb) - Extensión de PHP

Una ves configurado todo mongoDB en la máquina se debe crear una collection en la db test.

```sh
$ use test
$ db.createCollection('posts')
```
##### listo este paso se procede a instalar las dependencias de laravel

Instalar las dependencias de laravel
```sh
$ composer install
```
ejecutar servidor artisan
```sh
$ php artisan serve
```
luego ir a la URL
127.0.0.1:8000/

y probar la app :)

###### PD: Esta es mi primera vez usando vue.js
 
 

License
----

MIT
**Free Software**
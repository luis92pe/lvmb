<!DOCTYPE html>
<html>
<head>
    <title>LVMB Blog</title>
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://unpkg.com/vue-material@0.7.1/dist/vue-material.css">

    <script src="https://unpkg.com/vue"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.4"></script>
    <script src="https://unpkg.com/vue-material@0.7.1"></script>

    <style type="text/css">
        .main-content {
            padding: 16px;
        }
    </style>
</head>
<body>

    <div id="app">
        <md-toolbar>
            <h1 class="md-title">@{{message}}</h1>
        </md-toolbar>
        <div class="main-content">
            <md-layout md-gutter>
                <md-layout md-flex="80" v-show="listPosts">

                    <md-card v-for="item in someData"  >
                      <md-card-header>
                        <md-card-header-text>
                          <div class="md-title">@{{item.title}}</div>
                          <div class="md-subhead">@{{item.content}}</div>
                      </md-card-header-text>

                      <md-card-media>
                          <img src="http://www.hotel-r.net/im/hotel/nl/casa-nueva-3.jpg" alt="People">
                      </md-card-media>
                  </md-card-header>

                  <md-card-actions>
                    <md-button v-on:click.native="showPost(item._id) "  :idf="item" >Ver mas</md-button>
                </md-card-actions>
            </md-card>
            <md-spinner md-indeterminate v-show="spinner"></md-spinner>
        </md-layout>
    </md-layout>


    <md-layout md-column md-gutter v-show="showContent">

        <md-layout md-flex="20">
            <md-button  class="md-raised" v-on:click.native="reloadPosts">Regresar</md-button>
            <md-button v-on:click.native="editPost(editData._id) " >Editar</md-button>

        </md-layout>

        <md-layout md-flex="20">
            <img src="http://www.hotel-r.net/im/hotel/nl/casa-nueva-3.jpg" alt="People">
        </md-layout>

        <md-layout md-flex="20">
            <h1>@{{editData.title}}</h1>
        </md-layout>
        <md-layout>
            <p>@{{editData.content}}</p>
        </md-layout>
    </md-layout>
</div>





<md-dialog md-open-from="#fab" md-close-to="#fab" ref="dialog2">
  <md-dialog-title>@{{modaltitle}}</md-dialog-title>

  <md-dialog-content>
    
    <!-- form to create a new post -->
    <form v-show="newPostForm" novalidate @submit.stop.prevent="submit">
        <meta name="csrf-token" id="token" content="{{ csrf_token() }}">
        <legend>Nuevo Post</legend>
        <md-input-container>
            <label>Titulo</label>
            <md-input v-model="title"></md-input>
        </md-input-container>

        <md-input-container>
            <label>Contenido</label>
            <md-textarea v-model="content"></md-textarea>
        </md-input-container>
        <md-button class="md-raised md-primary" v-show="btnSavePostForm" v-on:click.native="savePost">Guardar</md-button>
        <md-button class="md-raised md-primary" v-show="btnUpdatePostForm" v-on:click.native="updatePost">Actualizar</md-button>

        <md-snackbar :md-position="vertical + ' ' + horizontal" ref="snackbar" :md-duration="duration">
            <span>@{{operationMsg}}.</span>
            <md-button class="md-accent" md-theme="light-blue" @click.native="$refs.snackbar.close()">Cerrar</md-button>
        </md-snackbar>
    </form>
</md-dialog-content>


</md-dialog>
<md-button class="md-fab md-fab-bottom-right" id="fab" @click.native="newPost()">
    <md-icon>add</md-icon>
</md-button>





</div>

<script type="text/javascript">

    Vue.use(VueMaterial);
    Vue.use(VueResource);

    var app = new Vue({
        el: '#app',
        data: {
            spinner: true,
            listPosts: true,
            newPostForm: false,
            btnSavePostForm: false,
            btnUpdatePostForm: false,
            showContent: false,
            message: 'LVMB',
            modaltitle: '',
            someData : [],
            title : '',
            content : '',
            editData : [],
            vertical: 'bottom',
            horizontal: 'center',
            duration: 4000,
            operationMsg: '',

        },
        ready: function(){
            this.reloadPosts();
        },
        methods: {
            reloadPosts: function(){
                console.log("consulta de api pokedeck")
                this.spinner = true;
                this.someData = [];
                this.showContent = false;

                this.$http.get('/posts').then(response => {
                         // get body data
                         this.someData = response.body;

                         this.spinner = false;
                         this.listPosts = true;

                     }, response => {
                        // error callback
                        console.log("Request Error");
                    });


            },

            newPost: function(){
                this.newPostForm = true;
                this.btnSavePostForm = true;
                this.title = '';
                this.content = '';
                this.modaltitle = 'Nuevo Post'
                this.openDialog('dialog2');

            },

            savePost: function(){
             this.$http.post('/posts', {title: this.title, content: this.content}).then(response => {

                    // get status
                    response.status;

                    // get status text
                    response.statusText;

                    // get 'Expires' header
                    response.headers.get('Expires');

                    // get body data
                    // this.someData = response.body;

                    this.operationMsg = "Nuevo Post creado correctamente"
                    this.$refs.snackbar.open();
                    this.newPostForm = false;
                    this.btnSavePostForm = false;
                    this.closeDialog('dialog2');
                    this.reloadPosts();


                }, response => {
                // error callback
            });
         },

         showPost: function(id){
            this.editData =  this.someData.filter(function(data) {
                return data._id == id;
            })[0];
            this.listPosts = false
            this.showContent = true;

        },

        updatePost: function(){
            this.$http.put('/posts/'+this.editData._id, {title: this.title, content: this.content, "_token": document.getElementById('token').content}).then(response => {

                            // get status
                            response.status;

                            // get status text
                            response.statusText;

                            // get 'Expires' header
                            response.headers.get('Expires');

                            // get body data
                            // this.someData = response.body;

                            this.operationMsg = "Edición del Post creado correctamente"
                            this.$refs.snackbar.open();
                            this.newPostForm = false;
                            this.btnUpdatePostForm = false;
                            this.editData.title = this.title
                            this.editData.content = this.content

                        }, response => {
                        // error callback
                    });
        },

        editPost: function(id){
            this.editData =  this.someData.filter(function(data) {
                return data._id == id;
            })[0];
            this.newPostForm = true;
            this.btnUpdatePostForm = true;

            this.title = this.editData.title;
            this.content = this.editData.content;
            this.btnSavePostForm = false
            this.modaltitle = 'Actualizar Post'

            this.openDialog('dialog2');
        },

        open() {
          this.$refs.snackbar.open();
      },
      openDialog(ref) {
        this.$refs[ref].open();
        
        

    },
    closeDialog(ref) {
      this.$refs[ref].close();
      this.newPostForm = false;
      this.btnSavePostForm = false;
  },
  onOpen() {
      console.log('Opened');
  },
  onClose(type) {
      console.log('Closed', type);
  }

},
computed: {
  filteredItems() {
    var self = this
    return this.items.filter(item => {
        return item.indexOf(self.filter) > -1
    })
}
}
});

app.reloadPosts();




</script>


</body>
</html>
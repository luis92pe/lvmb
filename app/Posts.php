<?php

namespace App;

use Moloquent\Eloquent\Model as Eloquent;

class Posts extends Eloquent
{
	protected $connection = 'mongodb';
    protected $collection = 'posts';
}
